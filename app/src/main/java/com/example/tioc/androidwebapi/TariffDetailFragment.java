package com.example.tioc.androidwebapi;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class TariffDetailFragment extends Fragment {
    private View view;
    private ExpandableListView expandableListView;
    private List<String>parentHeaderInformation;


    public TariffDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_tariff_detail, container, false);
        expandableListView = (ExpandableListView) view.findViewById(R.id.listResult);

        tariffDetail(getArguments().getString("ResultQuery"));
        return view;
    }

    private void tariffDetail(String result) {
        HashMap<String, List<String>> allChildItems = returnGroupedChildItems(result);
        ExpandableListViewAdapter expandableListViewAdapter = new ExpandableListViewAdapter(getContext(),
                parentHeaderInformation, allChildItems);
        expandableListView.setAdapter(expandableListViewAdapter);
    }

    private HashMap<String, List<String>> returnGroupedChildItems(String strJson){
        //fill title
        parentHeaderInformation = new ArrayList<String>();
        parentHeaderInformation.add("Product detail");
        parentHeaderInformation.add("Electricity");
        parentHeaderInformation.add("Electricity NetworkCost");
        parentHeaderInformation.add("Gas");
        parentHeaderInformation.add("Gas NetworkCost");

        HashMap<String, List<String>> childContent = new HashMap<String, List<String>>();
        try {
            JSONArray jsonMainNode = new JSONArray("["+strJson+"]");

            //Iterate the jsonArray and print the info of JSONObjects
            for(int i=0; i < jsonMainNode.length(); i++) {
                JSONObject jsonObject = jsonMainNode.getJSONObject(i);
                JSONObject tariffObject = jsonObject.getJSONObject("tariff");
                JSONObject tariffElectricityObject = jsonObject.getJSONObject("tariff").getJSONObject("electricityTariff");
                JSONObject tariffElectricityNetworkCostObject = jsonObject.getJSONObject("electricityNetworkCosts");
                JSONObject tariffGasObject = jsonObject.getJSONObject("tariff").getJSONObject("gasTariff");
                JSONObject tariffGasNetworkCostObject = jsonObject.getJSONObject("gasNetworkCosts");

                //fill content
                List<String> tContent = new ArrayList<String>();
                tContent.add("Product description: " + tariffObject.getString("productDescription").toString());
                tContent.add("KPG: " + tariffObject.getString("productDescription").toString());
                tContent.add("Tariff type: " + tariffObject.getString("tariffType").toString());
                tContent.add("Date Start: " + tariffObject.getString("dateStart").toString());
                tContent.add("Date End: " + tariffObject.getString("dateEnd").toString());
                tContent.add("Period: " + tariffObject.getString("period").toString());
                tContent.add("TaxDiscount: " + tariffObject.getString("taxDiscount").toString());
                tContent.add("Fixed Yearly Costs: " + tariffObject.getString("fixedYearlyCosts").toString());
                tContent.add("Total Monthly Costs: " + tariffObject.getString("totalMonthlyCosts").toString());
                tContent.add("Total Yearly Costs: " + tariffObject.getString("totalYearlyCosts").toString());
                childContent.put(parentHeaderInformation.get(0), tContent);

                //fill electricity content
                List<String> eContent = new ArrayList<String>();
                eContent.add("Enkel: " + tariffElectricityObject.getString("tariffSingle").toString());
                eContent.add("Hoog: " + tariffElectricityObject.getString("tariffNormal").toString());
                eContent.add("Laag: " + tariffElectricityObject.getString("tariffLow").toString());
                eContent.add("Jaarkosten vastrecht: " + tariffElectricityObject.getString("yearlyFixedRate").toString());
                eContent.add("Vastrecht: " + tariffElectricityObject.getString("fixedRate").toString());
                eContent.add("Jaarkosten: " + tariffElectricityObject.getString("yearlyCosts").toString());
                childContent.put(parentHeaderInformation.get(1), eContent);

                //fill electricity networkcost
                List<String> enContent = new ArrayList<String>();
                enContent.add("Periodieke aansluitvergoeding: " + tariffElectricityNetworkCostObject.getString("connectionFees").toString());
                enContent.add("Vaste leveringskosten transport: " + tariffElectricityNetworkCostObject.getString("fixedRateTransport").toString());
                enContent.add("Systeemdiensten: " + tariffElectricityNetworkCostObject.getString("systemServices").toString());
                enContent.add("Capaciteitstarief stroom: " + tariffElectricityNetworkCostObject.getString("capacityTariff").toString());
                enContent.add("Meterkosten: " + tariffElectricityNetworkCostObject.getString("meterFees").toString());
                childContent.put(parentHeaderInformation.get(2), enContent);

                //fill gas content
                List<String> gContent = new ArrayList<String>();
                gContent.add("Enkel: " + tariffGasObject.getString("tariff").toString());
                gContent.add("Vastrecht: " + tariffGasObject.getString("fixedRate").toString());
                gContent.add("Jaarkosten vastrecht: " + tariffGasObject.getString("yearlyFixedRate").toString());
                gContent.add("Jaarkosten: " + tariffGasObject.getString("yearlyCosts").toString());
                childContent.put(parentHeaderInformation.get(3), gContent);

                //fill gas networkcost
                List<String> gnContent = new ArrayList<String>();
                gnContent.add("Periodieke aansluitvergoeding: " + tariffGasNetworkCostObject.getString("connectionFees").toString());
                gnContent.add("Vaste leveringskosten transport: " + tariffGasNetworkCostObject.getString("fixedRateTransport").toString());
                gnContent.add("Systeemdiensten: " + tariffGasNetworkCostObject.getString("systemServices").toString());
                gnContent.add("Capaciteitstarief stroom: " + tariffGasNetworkCostObject.getString("capacityTariff").toString());
                gnContent.add("Meterkosten: " + tariffGasNetworkCostObject.getString("meterFees").toString());
                childContent.put(parentHeaderInformation.get(4), gnContent);
            }
            return childContent;
        } catch (JSONException e) {
            Toast.makeText(getContext(), "error" + e.toString(), Toast.LENGTH_SHORT).show();
            return null;
        }
    }
}
