package com.example.tioc.androidwebapi;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * A simple {@link Fragment} subclass.
 */
public class RegionFragment extends Fragment {
    EditText inputText;
    TextView resultText;
    Button save;
    View view;

    public RegionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_region, container, false);
        inputText = (EditText) view.findViewById(R.id.editText);
        resultText = (TextView) view.findViewById(R.id.textResults);
        save = (Button) view.findViewById(R.id.buttonGetRegion);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            //On click function
            public void onClick(View view) {
                Toast.makeText(getContext(), "please wait!", Toast.LENGTH_SHORT).show();
                updateMap();
            }
        });
        return view;
    }

    private void updateMap() {
        AsyncHttpClient client=new AsyncHttpClient();
        client.setTimeout(30*1000); //30s
        //fill params
        RequestParams params = new RequestParams();
        params.put("zipCode", inputText.getText().toString());
        client.get("" + getResources().getString(R.string.apiRegion) + "", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(String response) {
                try {
                    /*Toast.makeText(getContext(), "ok" + response, Toast.LENGTH_LONG).show();*/
                    initJson(response);

                } catch (IllegalStateException exception) {
                    Toast.makeText(getContext(), "error" + exception.toString(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Throwable throwable, String str) {
                super.onFailure(throwable, str);
                Toast.makeText(getContext(), "error" + throwable.toString() + str, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initJson(String strJson){
        String data = "";
        try {
            JSONArray jsonMainNode = new JSONArray("[" + strJson + "]");

            //Iterate the jsonArray and print the info of JSONObjects
            for(int i=0; i < jsonMainNode.length(); i++){
                JSONObject jsonObject = jsonMainNode.getJSONObject(i);

                String region = jsonObject.optString("region").toString();
                String eProvider = jsonObject.optString("electricityProvider").toString();
                String gProvider = jsonObject.optString("gasProvider").toString();

                data =  "Region code : "+ region +"<br/>" +
                        "Electricity Provider : "+ eProvider +"<br/>" +
                        "Gas Provider : "+ gProvider +"";
            }
            resultText.setText(Html.fromHtml(data));
        } catch (JSONException e) {
            Toast.makeText(getContext(), "error" + e.toString(), Toast.LENGTH_SHORT).show();
        }
    }
}
