package com.example.tioc.androidwebapi;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import static android.R.layout.simple_spinner_item;


/**
 * A simple {@link Fragment} subclass.
 */
public class TariffFragment extends Fragment {
    private Button save;
    private ExpandableListView expandableListView;
    private List<String>parentHeaderInformation;
    private View view;
    private Spinner spinnerProducts;
    private Spinner spinnerProductsType;
    private Spinner spinnerTariffChoice;
    private EditText editTextZipcode;
    private EditText editTextE;
    private EditText editTextEN;
    private EditText editTextEL;
    private EditText editTextG;

    //param value
    public String _productCode;
    public String _productType;
    public String _tariffChoice;

    public TariffFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_tariff, container, false);
        spinnerProducts = (Spinner) view.findViewById(R.id.spinnerProduct);
        spinnerProductsType = (Spinner) view.findViewById(R.id.spinnerProductType);
        spinnerTariffChoice = (Spinner) view.findViewById(R.id.spinnerTariffChoice);
        editTextZipcode = (EditText) view.findViewById(R.id.editTextZipcode);
        editTextE = (EditText) view.findViewById(R.id.editTextE);
        editTextEN = (EditText) view.findViewById(R.id.editTextEN);
        editTextEL = (EditText) view.findViewById(R.id.editTextEL);
        editTextG = (EditText) view.findViewById(R.id.editTextG);
        expandableListView = (ExpandableListView) view.findViewById(R.id.listResult);
        save = (Button) view.findViewById(R.id.buttonGetRegion);

       /*fill spinner from resources*/
        ArrayAdapter<CharSequence> adapterProduct = ArrayAdapter.createFromResource(getContext(), R.array.products, android.R.layout.simple_spinner_item);
        adapterProduct.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerProducts.setAdapter(adapterProduct);

        spinnerProducts.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                _productCode = spinnerProducts.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<CharSequence> adapterProductType = ArrayAdapter.createFromResource(getContext(), R.array.productTypes, android.R.layout.simple_spinner_item);
        adapterProductType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerProductsType.setAdapter(adapterProductType);

        spinnerProductsType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (spinnerProductsType.getSelectedItem().toString()) {
                    case "Groene Stroom":
                        _productType = "0";
                        break;
                    case "Groene Stroom en Gas":
                        _productType = "2";
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<CharSequence> adapterTariffChoice = ArrayAdapter.createFromResource(getContext(), R.array.tariffChoice, android.R.layout.simple_spinner_item);
        adapterTariffChoice.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTariffChoice.setAdapter(adapterTariffChoice);

        spinnerTariffChoice.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                /*FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.FILL_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT);*/
                switch(spinnerTariffChoice.getSelectedItem().toString()){
                    case "Standard":
                        editTextE.setVisibility(View.VISIBLE);
                        editTextEN.setVisibility(View.GONE);
                        editTextEL.setVisibility(View.GONE);
                        _tariffChoice = "0";
                        /*params.setMargins(0, 120, 0, 0);
                        save.setLayoutParams(params);*/
                        break;
                    case "Double":
                        editTextE.setVisibility(View.GONE);
                        editTextEN.setVisibility(View.VISIBLE);
                        editTextEL.setVisibility(View.VISIBLE);
                        _tariffChoice = "1";
                        /*params.setMargins(0, 150, 0, 0);
                        save.setLayoutParams(params);*/
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            //On click function
            public void onClick(View view) {
                Toast.makeText(getContext(), "please wait!", Toast.LENGTH_SHORT).show();

                updateMap();
            }
        });
        return view;
    }

    private void updateMap() {
        AsyncHttpClient client=new AsyncHttpClient();
        client.setTimeout(30*1000); //30s
        //fill params
        RequestParams params = new RequestParams();
        params.put("productCode", _productCode);
        params.put("productType", _productType);
        params.put("tariffChoice", _tariffChoice);
        params.put("zipCode", editTextZipcode.getText().toString().matches("") ? "1000AB" : editTextZipcode.getText().toString());
        params.put("electricitySingleValue", editTextE.getText().toString().matches("") ? "0" : editTextE.getText().toString());
        params.put("electricityNormalValue", editTextEN.getText().toString().matches("") ? "0" : editTextEN.getText().toString());
        params.put("electricityLowValue", editTextEL.getText().toString().matches("") ? "0" : editTextEL.getText().toString());
        params.put("gasValue", editTextG.getText().toString().matches("") ? "0" : editTextG.getText().toString());

        client.get("" + getResources().getString(R.string.apiTariff) + "", params, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            showDetail(response);
                            /*Toast.makeText(getContext(), "ok" + response, Toast.LENGTH_SHORT).show();
                            HashMap<String, List<String>> allChildItems = returnGroupedChildItems(response);
                            ExpandableListViewAdapter expandableListViewAdapter = new ExpandableListViewAdapter(getContext(),
                                    parentHeaderInformation, allChildItems);
                            expandableListView.setAdapter(expandableListViewAdapter);*/

                        } catch (IllegalStateException exception) {
                            Toast.makeText(getContext(), "error" + exception.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Throwable throwable, String str) {
                        super.onFailure(throwable, str);
                        Toast.makeText(getContext(), "error" + throwable.toString() + str, Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }

    /*private HashMap<String, List<String>> returnGroupedChildItems(String strJson){
        //fill title
        parentHeaderInformation = new ArrayList<String>();
        parentHeaderInformation.add("Product detail");
        parentHeaderInformation.add("Electricity");
        parentHeaderInformation.add("Electricity NetworkCost");
        parentHeaderInformation.add("Gas");
        parentHeaderInformation.add("Gas NetworkCost");

        HashMap<String, List<String>> childContent = new HashMap<String, List<String>>();
        try {
            JSONArray jsonMainNode = new JSONArray("["+strJson+"]");

            //Iterate the jsonArray and print the info of JSONObjects
            for(int i=0; i < jsonMainNode.length(); i++) {
                JSONObject jsonObject = jsonMainNode.getJSONObject(i);
                JSONObject tariffObject = jsonObject.getJSONObject("tariff");
                JSONObject tariffElectricityObject = jsonObject.getJSONObject("tariff").getJSONObject("electricityTariff");
                JSONObject tariffElectricityNetworkCostObject = jsonObject.getJSONObject("electricityNetworkCosts");
                JSONObject tariffGasObject = jsonObject.getJSONObject("tariff").getJSONObject("gasTariff");
                JSONObject tariffGasNetworkCostObject = jsonObject.getJSONObject("gasNetworkCosts");

                //fill content
                List<String> tContent = new ArrayList<String>();
                tContent.add("Product description: " + tariffObject.getString("productDescription").toString());
                tContent.add("KPG: " + tariffObject.getString("productDescription").toString());
                tContent.add("Tariff type: " + tariffObject.getString("tariffType").toString());
                tContent.add("Date Start: " + tariffObject.getString("dateStart").toString());
                tContent.add("Date End: " + tariffObject.getString("dateEnd").toString());
                tContent.add("Period: " + tariffObject.getString("period").toString());
                tContent.add("TaxDiscount: " + tariffObject.getString("taxDiscount").toString());
                tContent.add("Fixed Yearly Costs: " + tariffObject.getString("fixedYearlyCosts").toString());
                tContent.add("Total Monthly Costs: " + tariffObject.getString("totalMonthlyCosts").toString());
                tContent.add("Total Yearly Costs: " + tariffObject.getString("totalYearlyCosts").toString());
                childContent.put(parentHeaderInformation.get(0), tContent);

                //fill electricity content
                List<String> eContent = new ArrayList<String>();
                eContent.add("Enkel: " + tariffElectricityObject.getString("tariffSingle").toString());
                eContent.add("Hoog: " + tariffElectricityObject.getString("tariffNormal").toString());
                eContent.add("Laag: " + tariffElectricityObject.getString("tariffLow").toString());
                eContent.add("Vastrecht: " + tariffElectricityObject.getString("fixedRate").toString());
                eContent.add("Jaarkosten vastrecht: " + tariffElectricityObject.getString("yearlyFixedRate").toString());
                eContent.add("Jaarkosten: " + tariffElectricityObject.getString("yearlyCosts").toString());
                childContent.put(parentHeaderInformation.get(1), eContent);

                //fill electricity networkcost
                List<String> enContent = new ArrayList<String>();
                enContent.add("Periodieke aansluitvergoeding: " + tariffElectricityNetworkCostObject.getString("connectionFees").toString());
                enContent.add("Vaste leveringskosten transport: " + tariffElectricityNetworkCostObject.getString("fixedRateTransport").toString());
                enContent.add("Systeemdiensten: " + tariffElectricityNetworkCostObject.getString("systemServices").toString());
                enContent.add("Capaciteitstarief stroom: " + tariffElectricityNetworkCostObject.getString("capacityTariff").toString());
                enContent.add("Meterkosten: " + tariffElectricityNetworkCostObject.getString("meterFees").toString());
                childContent.put(parentHeaderInformation.get(2), enContent);

                //fill gas content
                List<String> gContent = new ArrayList<String>();
                gContent.add("Enkel: " + tariffGasObject.getString("tariff").toString());
                gContent.add("Vastrecht: " + tariffGasObject.getString("fixedRate").toString());
                gContent.add("Jaarkosten vastrecht: " + tariffGasObject.getString("yearlyFixedRate").toString());
                gContent.add("Jaarkosten: " + tariffGasObject.getString("yearlyCosts").toString());
                childContent.put(parentHeaderInformation.get(3), gContent);

                //fill gas networkcost
                List<String> gnContent = new ArrayList<String>();
                gnContent.add("Periodieke aansluitvergoeding: " + tariffGasNetworkCostObject.getString("connectionFees").toString());
                gnContent.add("Vaste leveringskosten transport: " + tariffGasNetworkCostObject.getString("fixedRateTransport").toString());
                gnContent.add("Systeemdiensten: " + tariffGasNetworkCostObject.getString("systemServices").toString());
                gnContent.add("Capaciteitstarief stroom: " + tariffGasNetworkCostObject.getString("capacityTariff").toString());
                gnContent.add("Meterkosten: " + tariffGasNetworkCostObject.getString("meterFees").toString());
                childContent.put(parentHeaderInformation.get(4), gnContent);
            }
            return childContent;
        } catch (JSONException e) {
            Toast.makeText(getContext(), "error" + e.toString(), Toast.LENGTH_SHORT).show();
            return null;
        }
    }*/

    private void showDetail(String result){
        Fragment fragment = new TariffDetailFragment();
        Bundle args = new Bundle();
        args.putString("ResultQuery", result);
        fragment.setArguments(args);

        //Inflate the fragment
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, fragment);
        ft.commit();
    }
}
