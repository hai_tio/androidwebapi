package com.example.tioc.androidwebapi;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class KnowledgeFragment extends Fragment  {
    private EditText inputText;
    private Button save;
    private ExpandableListView expandableListView;
    private List<String>parentHeaderInformation;
    private View view;

    //param value
    public String _searchQuery;

    public KnowledgeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_knowledge, container, false);
        inputText = (EditText) view.findViewById(R.id.editText);
        save = (Button) view.findViewById(R.id.buttonGetRegion);
        expandableListView = (ExpandableListView) view.findViewById(R.id.listResult);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            //On click function
            public void onClick(View view) {
                Toast.makeText(getContext(), "please wait!", Toast.LENGTH_SHORT).show();
                getApi();
            }
        });
        return view;
    }

    private void getApi() {
        AsyncHttpClient client=new AsyncHttpClient();
        client.setTimeout(30 * 1000); //30s

        if(inputText.getText().toString().matches("")) {
            client.get("" + getResources().getString(R.string.apiKnowledge) + "/popular", new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(String response) {
                            try {
                                /*Toast.makeText(getContext(), "ok" + response, Toast.LENGTH_SHORT).show();*/
                                HashMap<String, List<String>> allChildItems = returnGroupedChildItems(response);
                                ExpandableListViewAdapter expandableListViewAdapter = new ExpandableListViewAdapter(getContext(),
                                        parentHeaderInformation, allChildItems);
                                expandableListView.setAdapter(expandableListViewAdapter);

                            } catch (IllegalStateException exception) {
                                Toast.makeText(getContext(), "error" + exception.toString(), Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Throwable throwable, String str) {
                            super.onFailure(throwable, str);
                            Toast.makeText(getContext(), "error" + throwable.toString() + str, Toast.LENGTH_SHORT).show();
                        }
                    }
            );
        }else{
            //fill params
            RequestParams params = new RequestParams();
            params.put("categoryId", "0");
            params.put("searchQuery", inputText.getText().toString());
            params.put("totalResult", "10");
            _searchQuery = inputText.getText().toString();

            client.get("" + getResources().getString(R.string.apiKnowledge) + "", params, new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(String response) {
                            try {
                                /*Toast.makeText(getContext(), "ok" + response, Toast.LENGTH_SHORT).show();*/
                                HashMap<String, List<String>> allChildItems = returnGroupedChildItems(response);
                                ExpandableListViewAdapter expandableListViewAdapter = new ExpandableListViewAdapter(getContext(),
                                        parentHeaderInformation, allChildItems);
                                expandableListView.setAdapter(expandableListViewAdapter);

                            } catch (IllegalStateException exception) {
                                Toast.makeText(getContext(), "error" + exception.toString(), Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Throwable throwable, String str) {
                            super.onFailure(throwable, str);
                            Toast.makeText(getContext(), "error" + throwable.toString() + str, Toast.LENGTH_LONG).show();
                        }
                    }
            );
        }
    }

    private HashMap<String, List<String>> returnGroupedChildItems(String strJson){

        HashMap<String, List<String>> childContent = new HashMap<String, List<String>>();
        parentHeaderInformation = new ArrayList<String>();
        if(_searchQuery == null) {
            try {
                JSONArray jsonMainNode = new JSONArray(strJson);

                //Iterate the jsonArray and print the info of JSONObjects
                for (int i = 0; i < jsonMainNode.length(); i++) {
                    JSONObject jsonObject = jsonMainNode.getJSONObject(i);

                    //fill title
                    parentHeaderInformation.add(jsonObject.getString("title").toString());
                    String fTitle = jsonObject.getString("title").toString();

                    //fill content
                    List<String> fContent = new ArrayList<String>();
                    fContent.add(stripHtml(jsonObject.getString("content").toString()));
                    childContent.put(fTitle, fContent);
                }
                return childContent;
            } catch (JSONException e) {
                Toast.makeText(getContext(), "error" + e.toString(), Toast.LENGTH_SHORT).show();
                return null;
            }
        } else {
            try {
                JSONObject jsonMainNode = new JSONObject(strJson);
                JSONArray jsonChildNode = jsonMainNode.getJSONArray("results");

                //Iterate the jsonArray and print the info of JSONObjects
                for (int i = 0; i < jsonChildNode.length(); i++) {
                    JSONObject jsonObject = jsonChildNode.getJSONObject(i);

                    //fill title
                    parentHeaderInformation.add(jsonObject.getString("title").toString());
                    String fTitle = jsonObject.getString("title").toString();

                    //fill content
                    List<String> fContent = new ArrayList<String>();
                    fContent.add(stripHtml(jsonObject.getString("content").toString()));
                    childContent.put(fTitle, fContent);
                }
                return childContent;
            } catch (JSONException e) {
                Toast.makeText(getContext(), "error" + e.toString(), Toast.LENGTH_SHORT).show();
                return null;
            }
        }
    }

    public String stripHtml(String html) {
        return Html.fromHtml(html).toString();
    }
}
