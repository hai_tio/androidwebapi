package com.example.tioc.androidwebapi;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class DefaultFragment extends Fragment {
    private View view;
    private TextView networkStatus;
    private Boolean status;

    public DefaultFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_default, container, false);
        networkStatus = (TextView) view.findViewById(R.id.networkStatus);

        String status = "isNetworkAvailable:" + isNetworkAvailable().toString() + "<br />" +
                "isOnline:" + isOnline() + "<br />";

        networkStatus.setText(Html.fromHtml(status));
        return view;
    }

    private Boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    private boolean isOnline() {
        Runtime runtime = Runtime.getRuntime();
        try {
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int exitValue = ipProcess.waitFor();
            return (exitValue == 0);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }

    private void getResult() {
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(30 * 1000); //30s

        client.get("" + getResources().getString(R.string.apiKnowledge) + "/popular/0", new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            Toast.makeText(getContext(), "ok" + response, Toast.LENGTH_SHORT).show();
                        } catch (IllegalStateException exception) {
                            Toast.makeText(getContext(), "error" + exception.toString(), Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Throwable throwable, String str) {
                        super.onFailure(throwable, str);
                        Toast.makeText(getContext(), "error" + throwable.toString() + str, Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }
}